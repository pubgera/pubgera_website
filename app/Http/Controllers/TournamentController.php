<?php

namespace App\Http\Controllers;

use App\Classes\TourManager;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Tournament;
use App\Order;
use App\User;


class TournamentController extends Controller
{

	protected $TourManager;

	public function __construct(TourManager $TourManager,Order $Order ) {
        $this->TourManager = $TourManager;
        $this->Order = $Order;
    }

    public function GetTournament(Request $request)
    {
        
        $GetTournament = $this->TourManager->getAllTournament();

        return view('index', compact('GetTournament'));
    }

    public function GetTourDetails($tournamentid)
    {

        $GetTourDetails = $this->TourManager->GetTourDetails($tournamentid);

        return view('info', compact('GetTourDetails'));
    }

    public function Registration($tournamentid)
    {
        $pubg_leader_id = Auth::user()->pubg_leader_id;
        $pubg_leader_name = Auth::user()->pubg_leader_name;
        $email = Auth::user()->email;
        $mobileNumber = Auth::user()->mobile_number;

        $GetDetails = $this->TourManager->Registration($tournamentid, $mobileNumber);

        dd($GetDetails);
        return view('pages.join', compact('GetDetails','pubg_leader_name','email','mobileNumber', 'tournamentid', 'pubg_leader_id'));
    }

    public function UpdateUser(Request $request)
    {

        $mobileNumber = Auth::user()->mobile_number;    
        $leaderPubgId = $request->get('leaderPubgId');
        $leaderName = $request->get('leaderName');


        $this->user->UpdateUser($leaderPubgId, $leaderName, $mobileNumber);

        return back()->with('message', 'Update Successfully'); 
    }

    public function processOrder(Request $request)
    {

        $orderId = "TOUR" .rand(1111111111,9999999999);
        $tourId = $request->get('tourId');
        $mobileNumber = Auth::user()->mobile_number;


        $orderId = $this->Order->processOrder($orderId, $tourId, $mobileNumber);
        return response()->json(['status' => true, 'message' => 'order created successfully', 'orderId' => $orderId]);

    }
}
