<?php

namespace App\Http\Controllers;

use App\User;
use DateTime;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Common\Facades\Common;


class UserController extends Controller
{

    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
                
    }

    public function GetProfile()
    {
        $mobileNumber = Auth::user()->mobile_number;       
        $GetProfileDetails = $this->user->GetUser($mobileNumber);

        return view('pages.profile', compact('GetProfileDetails'));
    }

    

    public function verifyOtp(){

        return view('auth.verifyOtp');
    }

    public function signup(Request $request)
    {
        
        $this->validate($request, [
            'mobile_number' => 'required',
            'email' => 'required', 'string', 'email', 'max:255', 'unique:users',
            'password' => ['required', 'string', 'min:8', 'confirmed'],

        ]);

        $mobile_number = $request->get('mobile_number');
        $email = $request->get('email');
        $password = Hash::make($request->get('password'));
        

        try{
            
            $this->user->addUser($mobile_number, $email, $password);

        } catch (QueryExeption $ex){

                Log::info($ex->getMessage());
                return null;
            }

        // Check if user already exist, If not create it & send OTP for verification

        $user = $this->msg91Handler->sendOtp($mobile_number);
        dd($user);
        exit;
        if (isset($user)) {
            // User added, Send OTP for verification
            if (!$this->msg91Handler->sendOtp($mobileNumber)) {
                $result['status'] = false;
                $result['errors']['errorMessage'] = trans('users.error_while_sending_otp');
                $result['errors']['errorCode'] = ResponseErrorCode::INTERNAL_SERVER_ERROR;
                return Common::prepareRes($result, 500);
            }

            $result['status'] = true;
            $result['data']['message'] = trans('users.information_retrieved_successfully');
            $result['data']['user']['mobile_number'] = $mobileNumber;

            return Common::prepareRes($result, 200);
        } else {
            $result['status'] = false;
            $result['errors']['errorMessage'] = trans('users.error_while_creating_login_user');
            $result['errors']['errorCode'] = ResponseErrorCode::INTERNAL_SERVER_ERROR;
            return Common::prepareRes($result, 400);
        }
    }

    /**
     * @param Request $request
     * @return string
     * @throws \Illuminate\Validation\ValidationException
     */
    public function resendOtp(Request $request)
    {

        $this->validate($request, [
            'mobile_number' => 'required|phone',
        ]);

        $mobileNumber = $request->get('mobile_number');

        if (!$this->msg91Handler->resendOtp($mobileNumber)) {
            $result['status'] = false;
            $result['errors']['errorMessage'] = trans('users.error_while_sending_otp');
            $result['errors']['errorCode'] = ResponseErrorCode::INTERNAL_SERVER_ERROR;
            return Common::prepareRes($result, 500);
        }

        $result['status'] = true;
        $result['data']['message'] = trans('users.otp_resent_success');
        $result['data']['user']['mobile_number'] = $mobileNumber;

        return Common::prepareRes($result, 200);
    }

    /**
     * @param Request $request
     * @return string
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validateRegistration(Request $request) {
        $this->validate($request, [
            'mobile_number' => 'required|phone',
            'otp' => 'required|size:6',
            'client_token' => 'required',
            'device_name' => 'required',
            'device_id' => 'required',
        ]);

        $mobileNumber = $request->get('mobile_number');
        $otp = $request->get('otp');
        $password = $request->get('client_token');
        $deviceName = $request->get('device_name');
        $deviceId = $request->get('device_id');
        $clientIp = $request->ip();
        if($request->header("gps")) {
            try {
                $arr = array();
                $arr["user_id"] = $mobileNumber;
                $arr["gps_location"] = $request->header("gps");
                $arr["pagename"] = "validateRegistration";
                $arr["description"] = $deviceId;
                $arr["ip"] = $request->getClientIp();
                (new Gps_Location())->addlogs($arr);
            } catch (\Exception $exception) {
                Log::error($exception);
            }
        }
        // Check for OTP
        if ($this->msg91Handler->verifyOtp($mobileNumber, $otp)) {
            // Add OTP Log in database
            if (!$this->otpLogs->add($mobileNumber, $clientIp, true, $deviceName, $deviceId)) {
                $result['status'] = false;
                $result['errors']['errorMessage'] = trans('users.error_while_adding_otp_log');
                $result['errors']['errorCode'] = ResponseErrorCode::INTERNAL_SERVER_ERROR;
                return Common::prepareRes($result, 500);
            }

            // Update password with new one
            if (!$this->user->updatePassword($mobileNumber, $password)) {
                $result['status'] = false;
                $result['errors']['errorMessage'] = trans('users.error_while_changing_client_token');
                $result['errors']['errorCode'] = ResponseErrorCode::INTERNAL_SERVER_ERROR;
                return Common::prepareRes($result, 500);
            }

            // Generate token and return it
            // Check for mobile number and password
            if ($this->user->checkAuth($mobileNumber, $password)) {
                // Generate token and provide it
                $authToken = JWTAuth::attempt(['mobile_number' => $mobileNumber, 'password' => $password]);

                $result['status'] = true;
                $result['data']['message'] = trans('users.authentication_success');
                $result['data']['token'] = Crypt::encrypt($authToken);
                return Common::prepareRes($result, 200);
            } else {
                $result['status'] = false;
                $result['errors']['errorMessage'] = trans('users.authentication_failed');
                $result['errors']['errorCode'] = ResponseErrorCode::AUTHENTICATION_FAILED;
                return Common::prepareRes($result, 400);
            }

        } else {
            $result['status'] = false;
            $result['errors']['errorMessage'] = trans('users.otp_verification_failed');
            $result['errors']['errorCode'] = ResponseErrorCode::AUTHENTICATION_FAILED;
            return Common::prepareRes($result, 401);
        }
    }




}
