<?php
/**
 * Created by PhpStorm.
 * User: Prashant
 * Date: 30/03/2020
 * Time: 07:57 PM
 */

namespace App\Http\Controllers;


use App\Common\Facades\Common;
use Exception;
use GuzzleHttp\Exception\RequestException;
use function GuzzleHttp\Psr7\str;
use Illuminate\Support\Facades\Log;

class Msg91Handler
{

    private $apiKey;
    private $apiBaseUrl = 'https://control.msg91.com';
    private $client;

    public function __construct()
    {
        $this->client = Common::getHttpClient(['base_uri' => $this->apiBaseUrl]);
        $this->apiKey = '324925AHBmobmbVbMk5e7f3939P1';
    }

    public function sendOtp($mobileNumber) {
        $expireIn = env('OTP_EXPIRE_IN');
        $appName = env('APP_NAME');

        $data = [
            'mobile' => $mobileNumber,
            'message' => "##OTP## is your one time password to login at {$appName} application, Do not share your OTP with anyone. This OTP will expire in {$expireIn} minutes.",
            'sender' => 'CTBAPP',
            'otp_expiry' => 10,
            'otp_length' => 6,
            'country' => 91,
        ];

        $jsonResponse = $this->doHttpRequest('/api/sendotp.php', $data, 'GET');

        if (strcmp($jsonResponse->type, 'success') === 0) {
            return true;
        } else {
            Log::error('Error MSG91 Handler', [
                'class' => 'sendOtp',
                'function' => 'doHttpRequest',
                'line_no' => __LINE__,
                'error_message' => 'MSG91 Returned error',
                'http_response' => $jsonResponse,
            ]);

            return false;
        }
    }

    public function resendOtp($mobileNumber) {

        $data = [
            'mobile' => $mobileNumber,
            'retrytype' => 'text',
        ];

        $jsonResponse = $this->doHttpRequest('/api/v5/otp/retry', $data, 'POST');

        if (strcmp($jsonResponse->type, 'success') === 0) {
            return true;
        } else {
            Log::error('Error MSG91 Handler', [
                'class' => 'Msg91Handler',
                'function' => 'resendOtp',
                'line_no' => __LINE__,
                'error_message' => 'MSG91 Returned error',
                'http_response' => $jsonResponse,
            ]);

            return false;
        }

    }

    public function verifyOtp($mobileNumber, $otp) {
        $data = [
            'mobile' => $mobileNumber,
            'otp' => $otp
        ];

        $jsonResponse = $this->doHttpRequest('/api/v5/otp/verify', $data, 'POST');

        if (strcmp($jsonResponse->type, 'success') === 0) {
            return true;
        } else {
            Log::error('Error MSG91 Handler', [
                'class' => 'Msg91Handler',
                'function' => 'verifyOtp',
                'line_no' => __LINE__,
                'error_message' => 'MSG91 Returned error',
                'http_response' => $jsonResponse,
            ]);

            return false;
        }
    }

    private function doHttpRequest($endPoint, $data, $method) {
        try {
            //Add auth key in data
            $data = array_merge(['authkey' => $this->apiKey], $data);

            // Create option based on provided method
            $options = [];
            if (strcmp($method, 'GET') === 0) {
                $options['query'] = $data;
            } else {
                $options['form_params'] = $data;
            }

            // For Debug
            // $options['debug'] = fopen('php://stderr', 'w'); // Will show output in terminal

            $response = $this->client->request($method, $endPoint, $options);

            if ($response->getStatusCode() === 200) {
                return json_decode($response->getBody()->getContents());
            } else {
                Log::error('Error MSG91 Handler', [
                    'class' => 'Msg91Handler',
                    'function' => 'doHttpRequest',
                    'line_no' => __LINE__,
                    'error_message' => 'Received HTTP Status code other then 200',
                    'http_status_code' => $response->getStatusCode(),
                    'http_response' => $response->getBody()->getContents(),
                ]);

                $response = [ 'type' => 'error' ];
                return json_decode(json_encode($response));
            }

        } catch (RequestException $ex) {
            Log::error('Error MSG91 Handler', [
                'class' => 'Msg91Handler',
                'function' => 'doHttpRequest',
                'line_no' => __LINE__,
                'error_message' => $ex->getMessage(),
                'error_code' => $ex->getCode(),
                'http_response' => $ex->getResponse()->getBody()->getContents(),
            ]);

            $response = [ 'type' => 'error' ];
            return json_decode(json_encode($response));
        } catch (Exception $ex) {
            Log::error('Error MSG91 Handler', [
                'class' => 'Msg91Handler',
                'function' => 'doHttpRequest',
                'line_no' => __LINE__,
                'error_message' => $ex->getMessage()
            ]);

            $response = [ 'type' => 'error' ];
            return json_decode(json_encode($response));
        }
    }
}
