<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Classes\logActionManager;

class PricePool extends Model
{
    protected $connection = 'mysql';
    protected $table = 'tbl_price_pool';
    public $timestamps = false;


    // public $timezone = 'Asia/Kolkata';

    public function getPricePool($tournament_id)
    {
        try {

            $getPricePool = $this->where('tour_id', $tournament_id)->get();
            
            
            if($getPricePool) {
                return $getPricePool;
            }
            return null;
        } catch(QueryException $queryException) {
            Log::error('PricePool Model', ['getPricePool' => $queryException->getMessage()]);
            return null;
        }
    }  
    
}
