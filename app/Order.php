<?php

namespace App;


use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Order extends Model
{
    use Notifiable;

    protected $table = "tbl_orders";
    public $timestamps = false;
    public $timezone = 'Asia/Kolkata';

    public function processOrder($orderId, $tourId, $mobileNumber){

        try{

            $this->order_id = $orderId;
            $this->tour_id = $tourId;
            $this->mobile_no = $mobileNumber;

            if($this->save()) {
                return $this->order_id;
            }
            return null;

        } catch (QueryExeption $ex){

            Log::info($ex->getMessage());
            return null;
        }
    }

}
