<?php

namespace App\Classes;


use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Tournament;
use App\PricePool;


class TourManager
{
	protected $User;
	protected $Tournament;
    protected $PricePool;


	public function __construct(User $User, Tournament $Tournament, PricePool $PricePool) {
        $this->User = $User;
        $this->Tournament = $Tournament;
        $this->PricePool = $PricePool;
       
    }

    public function getAllTournament()
    {
    	try {
            $TourDetails = $this->Tournament->getTournament();

            if($TourDetails) {
                return $TourDetails;
            }
            return null;
        } catch(\Exception $exception) {
            Log::critical('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            Log::error('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            return null;
        }
    }

    public function GetTourDetails($tournamentid)
    {
        try {
            $GetTourDetails['TourDetails'] = $this->Tournament->GetTourDetails($tournamentid);
            $GetTourDetails['TourPricePool'] = $this->PricePool->getPricePool($tournamentid);
            

            if($GetTourDetails) {
                return $GetTourDetails;
            }
            return null;
        } catch(\Exception $exception) {
            Log::critical('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            Log::error('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            return null;
        }
    }

    public function Registration($tournamentid, $mobileNumber)
    {
        try {
            $GetTourDetails['TourDetails'] = $this->Tournament->GetTourDetails($tournamentid);
            $GetTourDetails['GetUserDetails'] = $this->User->GetUser($mobileNumber);
            
            
            if($GetTourDetails) {
                return $GetTourDetails;
            }
            return null;
        } catch(\Exception $exception) {
            Log::critical('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            Log::error('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            return null;
        }
    }

}
