<?php
/**
 * Created by PhpStorm.
 * User: Prashant
 * Date: 20/03/2020
 * Time: 12:31 PM
 */

namespace App\Common;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Crypt;

class Common
{
    function prepareRes($result, $statusCode = 200, $customHeader = [])
    {
        return Response()->json($result)->setStatusCode($statusCode);
    }

    function generateRandomString($length = 10) : string {
        $characters = '123456789';
        //todo removethis
       // $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return strtoupper($randomString);
    }

    function getHttpClient(array $config = [], $isSetProxy = false): Client {
        $apiClient = null;

        if ($isSetProxy === true) {
            if(is_array($config) && sizeof($config) > 0) { //Check the size of an array
                if(!isset($config['proxy'])) { //if
                    $config['proxy'] = ['https' => env('PROXY_SERVER')];
                }
            } else {
                //No configuration is provided set just proxy because this is live env
                $config['proxy'] = ['https' => env('PROXY_SERVER')];
            }
        }
        $apiClient = new Client($config);
        return $apiClient;
    }
}
