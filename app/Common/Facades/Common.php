<?php
/**
 * Created by PhpStorm.
 * User: Prashant
 * Date: 20/03/2020
 * Time: 12:32 PM
 */

namespace App\Common\Facades;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Facade;

/**
 * @method static string prepareRes(array $result, int $statusCode, array $customHeader =[])
 * @method static Client getHttpClient(array $config = [], $isSetProxy = true)
 * @method static string generateRandomString($length = 8)
 */

class Common extends Facade
{
    protected static function getFacadeAccessor() { return 'common'; }
}