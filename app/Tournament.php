<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Classes\logActionManager;

class Tournament extends Model
{
    protected $connection = 'mysql';
    protected $table = 'create_tournament';
    public $timestamps = false;


    // public $timezone = 'Asia/Kolkata';

    public function getTournament()
    {
        try {
            $TourDetails = $this->orderBy('timestamp', 'desc')
        					    ->get();

            

            if($TourDetails) {
            	return $TourDetails;
            }
            Log::error('Error while getting bank details');
            return null;
        } catch (QueryException $queryException) {
            Log::critical('BankDetail Model', ['getBankDetails' => $queryException->getMessage()]);
            Log::error('BankDetail Model', ['getBankDetails' => $queryException->getMessage()]);
            return null;
        }
    }

    public function GetTourDetails($tournamentid)
    {
        try {

            $GetTourDetails = $this->where('tournament_id', $tournamentid)->first();

            if($GetTourDetails) {
                return $GetTourDetails;
            }
            Log::error('Error while getting bank details');
            return null;
        } catch (QueryException $queryException) {
            Log::critical('BankDetail Model', ['getBankDetails' => $queryException->getMessage()]);
            Log::error('BankDetail Model', ['getBankDetails' => $queryException->getMessage()]);
            return null;
        }
    }   
    
}
