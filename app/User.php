<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = "users";
    public $timestamps = false;
    public $timezone = 'Asia/Kolkata';

    public function addUser($mobile_number, $email, $password){

        try{
            
            $this->mobile_number = $mobile_number;
            $this->email = $email;
            $this->password = $password;

            if($this->save()) {
                return true;
            }
            return null;

            } catch (QueryExeption $ex){

                Log::info($ex->getMessage());
                return null;
            }
    }

    public function UpdateUser($leaderPubgId, $leaderName ,$mobileNumber ){

        try {
            if($this->where('mobile_number', $mobileNumber)->update(['pubg_leader_id' => $leaderPubgId ,'pubg_leader_name' => $leaderName ]) === true) {
                return true;
            }
            return false;
        } catch (QueryException $exception) {
            Log::error('KycUser Model -> updateUser', [
                'Error' => $exception->getMessage(),
                'File' => $exception->getFile(),
                'Line' => $exception->getLine()
            ]);
            return false;
        }
    }

    public function GetUser($mobileNumber){

        try {

                $GetUser = $this->where('mobile_number', $mobileNumber)->get();

            if($GetUser) {
                return $GetUser;
            }
            return null;
        } catch (QueryException $exception) {
            Log::error('KycUser Model -> updateUser', [
                'Error' => $exception->getMessage(),
                'File' => $exception->getFile(),
                'Line' => $exception->getLine()
            ]);
            return false;
        }
    }

    public function CheckUser($mobile_number) {
        try {
            $user = $this->first(['mobile_number' => $mobile_number], false);

            if (isset($user) && !empty($user)) {
                return $user;
            }
        } catch (QueryExeption $ex){

                Log::info($ex->getMessage());
                return null;
            }
    }

    public function setPasswordAttribute($password)
{
    $this->attributes['password'] = Hash::make($password);
}

    protected $fillable = [
        'mobile_number', 'email', 'password',
    ];

    protected $hidden = [
        'password'  , 'remember_token',
    ];

    protected $casts = [
        'verify_date' => 'datetime',
    ];
}
