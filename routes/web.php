<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();


Route::get('/profile', function () { return view('pages.profile');});
Route::get('/myteam', function () { return view('pages.myteam');});


Route::post('/signup', 'UserController@signup');
Route::get('/join', function () { return view('pages.join');});

/* Users Routes*/
Route::get('/profile', 'UserController@GetProfile'); 



/* Tournament Routes*/
Route::get('/', 'TournamentController@GetTournament');
Route::get('/tournament/{tournamentid}', 'TournamentController@GetTourDetails')->name('tournament');
Route::post('/info', 'TournamentController@Info');   
Route::post('/processorder', 'TournamentController@processOrder');
Route::get('/registration/{tournamentid}', 'TournamentController@Registration'); 

/*Route::get('/verifyOtp', 'UserController@verifyOtp');*/