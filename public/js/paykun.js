// Create Paykun object
// Currently Sandbox does not support JS checkout, and so you will not be able to test JS integration, 
// isLive can only be true for now


  const pk = new PayKun({ merchantId : "181970219643848", accessToken: "1EDFC75658B15051A12CFE459AE2CBF4", isLive: true});
// Add this function in your javascript tag or your js file
// This method can be called to initialize payment, It can be any event User Generated Or system Generated
  function initPayment(tour_entry_fee,pubg_leader_name,email,mobileNumber){

      let order = {
          amount: tour_entry_fee, // Amount to collect
          orderId: "PUBG" + (new Date).getTime(), // Unique order id, You can use your custom login here, but make sure it generates unique ID everytime
          productName: "PubgEra", // Name of the product
          customerName: pubg_leader_name, // Name of the customer
          customerEmail: email, // Email of the customer
          customerMobile: mobileNumber,// Mobile of customer
          currency: "INR",  //set your 3 digit currency code here
          // Following are callback function and will be called when any result is received after payment, 
          // If payment is success then onSuccess method will be called Or else onCancelled method will be called
          
          onSuccess: function (transactionId) {
              // You can use 'transactionId' variable to process payment at your server side if you like, 
              // In that case you can call our transaction status API on your server to get transaction information Or
              // You can get complete transaction details by calling the following func tion
              // WARNING: It is advisable to verify transaction amount and status at your server side using Transaction ID before delivering any servianyce for security reason
              var transaction = pk.getTransactionDetail(transactionId, function(transaction) {
                  // You can show payment success message to user here, Also process this payment success at your server side to deliver services to customer
                  console.log(transaction);
                  alert('Payment is success, Your transaction ID : ' + transaction.transaction.payment_id);
              });

          },
          onCancelled: function (transactionId) {
              // You can use 'transactionId' variable to process payment at your server side if you like, 
              // In that case you can call our transaction status API on your server to get transaction information Or
              // You can get complete transaction details by calling following function
              var transaction = pk.getTransactionDetail(transactionId, function(transaction) {
                  // You can show payment canceled message to user here, Also mark this payment as failed/cancelled at your server side
                  console.log(transaction);
                  /*alert('Payment is cancelled , Your transaction ID : ' + transaction.transaction.payment_id);*/
                      $.ajax({
                        type: "POST",
                        url:"/ajaxRequestpost",
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {"transaction": transaction},
                        success: function(response){

                          
                             window.location.href = "/paytobank";
                        }
                  });
                  saveData.error(function() { alert("Something went wrong"); });
              

              });
          }
      };
      //Init Paykun Payment and open checkout popup
      pk.init(order);
  }


/*function getid()
{
  var tid=document.getElementById('transaction_id').value;
    alert(getcityvalue(tid));
  return confirm('Are you sure you want to delete')
}


function getdata(cityid){
      // this will generate another thread to run in another function
      jQuery.ajax({
          url: '/payment/' + tid,
          type: 'get',
          dataType: 'text/html',
          success: function(data) {
              return(data);
          }, 
          error: function() {
             return "Hello";
          }
      });
  }

*/