@extends('layouts.master')

@section('content')

      <!-- Header Area End -->
       
       
      <!-- Slider Area Start -->
      <section class="slider-area">
         <div class="fag-slide owl-carousel">
            <div class="fag-main-slide slide-1">
               
            </div>
            <div class="fag-main-slide slide-2">
              
            </div>
            <div class="fag-main-slide slide-3">
              
            </div>
           
         </div>
      </section>
      <!-- Slider Area End -->
       
      <!-- Games Area Strat -->
      <section class="fag-games-area section_140">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="site-heading">
                     <h2 class="heading_animation">Popular <span>Tournaments</span></h2>
                
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12">
                  <div class="games-masonary">
                     <div class="projectFilter project-btn">
                        <ul>
                           
                           <li><a href="#" data-filter=".free" class="current">Upcoming</a></li>
                           <li><a href="#" onclick='window.location="tournaments.html"'>show all</a></li>
                          
                        </ul>
                     </div>
                     <div class="clearfix gamesContainer">
                         @foreach($GetTournament as $data)
                              <div class="games-item free">
                                 <div class="games-single-item img-contain-isotope">
                                    <div class="games-thumb">
                                       <div class="games-thumb-image">
                                          <a href="#">
                                             <img src="assets/img/free-solo.jpg" alt="games" />
                                          </a>
                                       </div>
                                    
                                    </div>
                                    <div class="games-desc">
                                       <h3><a href="#">{{ $data->tour_name}}</h3>
                                       <p class="game-meta">MAP: ERANGLE [TPP]</p>
                                       <hr>
                                       <p class="game-meta">Date: <span>{{ $data->tour_date}}</span></p>
                                       <p class="game-meta">Time: <span>{{ $data->tour_date}}</span></p>
                                       <p class="game-meta">Slots: <span>/{{ $data->tour_player}}</span></p>
                                       <p class="game-meta">Total Prize Pool: <span>₹{{ $data->tour_price_pool}}</span></p>
                                       <div class="game-action">
                                          <div class="game-price">
                                             <h4>₹{{ $data->tour_player}}</h4>
                                          </div>
                                          <div class="game-buy">
                                           <a href="{{ URL::to('/tournament/'.$data->tournament_id) }}" class="fag-btn-outline">{{ $data->tour_status}}</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                        @endforeach
                              
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      @stop
      <!-- Games Area End -->

      <!-- Footer Area Start -->

