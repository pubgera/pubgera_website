
   @include ('layouts.include.head')

   @include ('layouts.include.header')
        <!-- Header Area End -->

        <!-- page 404 -->
        <div class="login section--full-bg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="page-404__wrap">
                            <div class="login-wrapper">
                                <h3>Create Account</h3>

                                <form method="POST" action="{{ route('register') }}">
                                    @csrf
                                    <div class="form-row">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email Address" name="email" value="{{ old('email') }}" required autocomplete="email">
                                    </div>
                                    <div class="form-row">    
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                    </div>
                                    <div class="form-row">
                                        
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                    </div>
                                    <div class="form-row">
                                        
                                        <input id="password-confirm" type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                    <div class="form-row">
                                        <input type="text" placeholder="+91" name="mobile_number" id="mobileNumber" value="" required />
                                    </div>

                                    <div class="form-row"></div>
                                    <div class="form-row">
                                        <button class="fag-btn" type="submit" id="submit">Create your Account!</button>
                                    </div>
                                    OR
                                    
                                </form>                                    
                                <div class=""><a href="login">
                                    <button class="fag-btn" id="">Login to your Account!</button></a>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page 404 -->

        <!-- Footer Area Start -->
        @include ('layouts.include.footer')
