https://daddydope.in/registration?success=200&id=8719
<!DOCTYPE html>
<html lang="en-US">
   <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="DaddyDope | ESports gaming company">
    <meta name="keyword" content="game, gaming, videogame, developer, steam, studio, team">
    
    <!-- Title -->
    <title>DaddyDope</title>
    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon/favicon-32x32.png">
    <!--Bootstrap css-->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/datatables.min.css" rel="stylesheet">
    <!--Font Awesome css-->
    <link href="/assets/fontawesome/css/all.min.css"  rel="stylesheet">
    <link href="assets/css/font-awesome.min.css"  rel="stylesheet">
    <!--Magnific css-->
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <!--Owl-Carousel css-->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
    <!--NoUiSlider css-->
    <link rel="stylesheet" href="assets/css/nouislider.min.css">
    <!--Animate css-->
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <!--Site Main Style css-->
    <link rel="stylesheet" href="assets/css/style.css">
    <!--Responsive css-->
    <link rel="stylesheet" href="assets/css/responsive.css">
    <style>
        .form-control[readonly]{
            background-color:#818181 !important;
        }
    </style>
 </head>
   <body>
       
       
      
       
  <!-- Header Area Start -->
  <nav class="fag-header navbar navbar-expand-lg">
    <div class="container">
       <a class="navbar-brand" href="/"><img src="assets/img/logo.png" alt="site logo" /></a>
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
       <span class="menu-toggle"></span>
       </button>
       <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="header_menu  mr-auto">
             <li class="nav-item active">
                <a href="/" class="nav-link">Home</a>
             </li>
             <li class="nav-item active">
               <a href="/tournaments" class="nav-link">Tournaments</a>
            </li>
             <li class="nav-item">
                <a href="#footerTag" class="nav-link">Contact</a>
             </li>
          </ul>
          <div class="header-right  my-2 my-lg-0">            
             
                <div class="header-auth  nav-item dropdown">
                    <a class="lang-btn nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                    My Account</a>
                    <ul class="user_menu dropdown-menu">
                       <li><a href="/profile">Profile</a></li>
                       <li><a href="/myEarnings">My Earnings</a></li>
                       <li><a href="/myTeam">My Team</a></li>
                       <li><a href="/myTournaments">My Tournaments</a></li>
                       <li><a href="/settings">Settings</a></li>
                       <li><a href="/logout">Logout</a></li>
                    </ul>
                 </div>
             
             
          </div>
       </div>
    </div>
 </nav>
  <!-- Header Area End -->
       
      <!-- page 404 -->
      <div class="page-404 section--full-bg" style='background: #2C2844 !important;'>
         <div class="container-fluid">
            <div class="row">
               <div class="offset-md-2 col-md-8">
                  <div class="page-404__wrap">
                     <div class="page-404__content" style='align-items: unset; color:white;'>
                        <h2>You have Successfully registered for</h2>
                        <br>
                          <h4 style="color:red">We will SMS you the <span style="color:orange">Team number</span> and  <span style="color:orange">Room Id & Password</span> on your registered mobile number 10 mins before the match starts.</h4>
                        <br>
                        <h4><b>Tournament :</b> “SOLO - ULTIMATE“</h4>
                        <br>
                
                        <br>
                        <h4><b>Your unique Registration number :</b> 8719</h4>
                    
                  
                       <br>
                       <h4>You can also retrieve your Room ID under my tournments.</h4>
                       <br>
                       <h3>GENERAL RULES:</h3>
              
                       <ol >
                           <li>- DO NOT SHARE THE ROOM DETAILS WITH ANYONE.</li>
                           <li>- DO NOT USE HACKING PLUG-INS</li>
                           <li>- DO NOT USE PC EMULATOR</li>
                           <li>- NO TEAMUPS</li>
                       </ol>
                       <br>
                       <h2>OUR TEAM SPECTATE EACH AND EVERY MATCH & IF YOU FOUND GUILTY WE WILL SUSPEND YOUR ACCOUNT
                           PERMANENTLY. AND OUR DECISION IN THESE CASES ARE FINAL.</h2>
                           <br>
                        <a href="/" class="fag-btn">back to home</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- end page 404 -->
       
  <!-- Footer Area Start -->
  <footer class="fag-footer">
    <div class="footer-top-area">
       <div class="container">
          <div class="row">
             <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="single-footer">
                   <h3>About us</h3>
                   <p>Daddydope is an eSports gaming company. We organize eSports or Competitive Gaming Tournaments which are played across many platforms such as Mobile , PC.</p>
                 
                </div>
             </div>
             <div class="col-lg-5 col-md-6 col-sm-12">
                <div class="widget-content">
                   <div class="row clearfix">
                      <div class=" col-lg-6 col-md-6 col-sm-12">
                         <div class="single-footer">
                            <h3>Our Games</h3>
                            <ul>
                               <li><a href="#"><span class="fa fa-caret-right"></span>
                              Pubg Mobile</a></li>
                           
                             
                            </ul>
                         </div>
                      </div>
                      <div class=" col-lg-6 col-md-6 col-sm-12">
                         <div class="single-footer" id="footerTag">
                            <h3>Explore</h3>
                            <ul>
                               <li><a href="#"><span class="fa fa-caret-right"></span>About</a></li>
                               <li><a href="#"><span class="fa fa-caret-right"></span>Tournaments</a></li>
                               <li><a href="#"><span class="fa fa-caret-right"></span>FAQ</a></li>
                               <li><a href="#"><span class="fa fa-caret-right"></span>Privacy Policy</a></li>
                               <li><a href="#"><span class="fa fa-caret-right"></span>Terms of Service</a></li>
                            </ul>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
             <div class=" col-lg-3 col-md-6 col-sm-12">
                <div class="single-footer">
                   <h3>Contact Us</h3>
                  
                   <div class="footer-contact">
                      <h4 class="title"><i class="fa fa-pencil"></i>Email Address</h4>
                      <p>support@daddydope.in</p>
                   </div>
                   
                </div>
             </div>
          </div>
       </div>
    </div>
    <div class="footer-bottom">
       <div class="container">
          <div class="row">
             <div class="col-12">
                <div class="footer-bottom-inn">
                   <div class="footer-logo">
                      <a href="index.html">
                      <img src="assets/img/logo.png" alt="site logo" />
                      </a>
                   </div>
                   <div class="footer-social">
                      <ul>
                         <li><a href="http://facebook.com/daddydopeyt"><span class="fa fa-facebook"></span></a></li>
                         <li><a href="https://www.instagram.com/daddy_dopeyt"><span class="fa fa-instagram"></span></a></li>
                         <li><a href="https://www.youtube.com/c/DaddyDopeYT"><span class="fa fa-youtube"></span></a></li>
                         <li><a href="https://twitter.com/daddydopeyt"><span class="fa fa-twitter"></span></a></li>
                        
                      </ul>
                   </div>
                   <div class="copyright">
                      <p>&copy; Copyrights 2020 DADDYDOPE - All Rights Reserved</p>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </div>
 </footer>
  <!-- Footer Area End -->
       
      <!--Jquery js-->
      <script src="assets/js/jquery.min.js"></script>
      <!-- Popper JS -->
      <script src="assets/js/popper.min.js"></script>
      <!--Bootstrap js-->
      <script src="assets/js/bootstrap.min.js"></script>
      <!--Owl-Carousel js-->
      <script src="assets/js/owl.carousel.min.js"></script>
      <!--Magnific js-->
      <script src="assets/js/jquery.magnific-popup.min.js"></script>
      <!--wNumb js-->
      <script src="assets/js/wNumb.js"></script>
      <!--NoUiSlider js-->
      <script src="assets/js/nouislider.min.js"></script>
      <!-- Isotop Js -->
      <script src="assets/js/isotope.pkgd.min.js"></script>
      <script src="assets/js/custom-isotop.js"></script>
      <!-- Counter JS -->
      <script src="assets/js/jquery.counterup.min.js"></script>
      <!-- Way Points JS -->
      <script src="assets/js/waypoints-min.js"></script>
      <!--Main js-->
      <script src="assets/js/main.js"></script>
   </body>
</html>

