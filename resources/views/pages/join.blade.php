@extends('layouts.master')

@section('content')

<section class="fag-game-page section_100">
<div class="container">
    <div class="row">
        <div class="offset-md-4 col-md-4">
            <h1 style="color: white;">Registration Details</h1>
        </div>
    </div>
    @if (Session::has('message'))
       <div class="alert alert-success" role="alert">
           {{Session::get('message')}}
       </div>
    @endif
    <div class="row">
        <div class="col-md-8 offset-md-2 checkout-left-box">
            <div class="alert alert-danger" id="err" role="alert" style="display: none;"></div>

            <form method="POST" action="/updateuser" class="needs-validation">
                 @csrf

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pubgID" class="col-form-label">Leader PUBG ID </label>
                                <input type="number" class="form-control" id="leaderPubgId" name="leaderPubgId" {{!empty($pubg_leader_id) ? 'disabled': ''}} value="{{$pubg_leader_id}}"  />
                                <div class="invalid-feedback" id="leaderPubgIdFeedback"></div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="leaderName" class="col-form-label">Leader PUBG In-Game NAME</label>
                                <input type="text" class="form-control" id="leaderName" name="leaderName" {{!empty($pubg_leader_name) ? 'disabled': ''}} value="{{$pubg_leader_name}}" />
                            </div>
                        </div>

                        {{--<div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pubgID" class="col-form-label">Leader PUBG ID</label>
                                <input class="form-control" value="{{ $data['pubg_leader_id'] }}" readonly=""/>
                                <div class="invalid-feedback" id="leaderPubgIdFeedback"></div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="leaderName" class="col-form-label">Leader PUBG In-Game NAME</label>

                                <input class="form-control" value="{{ $data['pubg_leader_name'] }}"  readonly="" />
                            </div>
                        </div>--}}

                
                <!-- <div class="form-group">
                    <label for="leaderNumber" class="col-form-label">Leader's Whatsapp Number</label>
                    <input type="number" class="form-control" id="leaderNumber" name="leaderNumber" required="" />
                    <div class="invalid-feedback" id="numberFeedback"></div>
                </div> -->
                <br/>

                <p style="color: #d9603f;">PUBG Mobile in-game name and Game ID submitted on DaddyDope should match or else the player won't receive any reward.</p>
                <p style="color: #d9603f;">If we caught you sharing Room ID and Password with other players who are not registered with us . Your Daddydope account will be banned permanently.</p>

                <div class="form-row">


                        
                </div>
                <br/>
                {{--<div class="form-row">
                    <button class="fag-btn" id="back">Back</button>
                </div>--}}

                {{--@foreach($GetDetails['TourDetails'] as $data)
                    <button onclick="processOrder()" class="fag-btn"> {{ $data['tournament_id'] }} Registers Now</button>
                @endforeach--}}
                    <button type="button" onclick="processOrder()" class="fag-btn "> Registers Now</button>
            </form>

        </div>

           
    </div>
</div>
@endsection

@section('customJs')

<script src="https://checkout.paykun.com/checkout/plugin/crypt/crypto-js.min.js"></script>
<script src="https://checkout.paykun.com/checkout/js/paykun.js"></script>
<script src="{{ asset('js/paykun.js') }}"></script>
        
<script type="text/javascript">


function csf() {
    csrf = $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    return csrf;
}

        function processOrder() {

            var leaderPubgId = $('#leaderPubgId').val();
            var leaderName = $('#leaderName').val();
            var tourId = "{{$tournamentid}}";
            csf();

            $.ajax({
                url:'/processorder',
                type:'post',
                data : {leaderPubgId: leaderPubgId, leaderName : leaderName, tourId : tourId},
                dataType  : 'json',
                success:function(response){
                    if(response && response.status == true) {
                        initPayment('{{}}', response.orderId);
                    }
                },

            });
        }


/*    function processOrder() {
        var trounament_id =hgfdhf,
        

    }*/
</script>
@endsection