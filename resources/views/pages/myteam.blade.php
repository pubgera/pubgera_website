
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="DaddyDope | ESports gaming company">
    <meta name="keyword" content="game, gaming, videogame, developer, steam, studio, team">
    
    <!-- Title -->
    <title>DaddyDope - MyTeam</title>
    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon/favicon-32x32.png">
    <!--Bootstrap css-->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/datatables.min.css" rel="stylesheet">
    <!--Font Awesome css-->
    <link href="/assets/fontawesome/css/all.min.css"  rel="stylesheet">
    <link href="assets/css/font-awesome.min.css"  rel="stylesheet">
    <!--Magnific css-->
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <!--Owl-Carousel css-->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
    <!--NoUiSlider css-->
    <link rel="stylesheet" href="assets/css/nouislider.min.css">
    <!--Animate css-->
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <!--Site Main Style css-->
    <link rel="stylesheet" href="assets/css/style.css">
    <!--Responsive css-->
    <link rel="stylesheet" href="assets/css/responsive.css">
    <style>
        .form-control[readonly]{
            background-color:#818181 !important;
        }
    </style>
 </head>

<body>



  <div id="spinner" class='align-middle'
    style='width:100%;display:none;align-items:center;justify-content: center;text-align: center;left: 50;height: 100%;background-color: grey;position: absolute;z-index: 1;opacity: 0.5;'>

    <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">

    </div>
  </div>

  <!-- Header Area Start -->
  <nav class="fag-header navbar navbar-expand-lg">
    <div class="container">
       <a class="navbar-brand" href="/"><img src="assets/img/logo.png" alt="site logo" /></a>
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
       <span class="menu-toggle"></span>
       </button>
       <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="header_menu  mr-auto">
             <li class="nav-item active">
                <a href="/" class="nav-link">Home</a>
             </li>
             <li class="nav-item active">
               <a href="/tournaments" class="nav-link">Tournaments</a>
            </li>
             <li class="nav-item">
                <a href="#footerTag" class="nav-link">Contact</a>
             </li>
          </ul>
          <div class="header-right  my-2 my-lg-0">            
             
                <div class="header-auth  nav-item dropdown">
                    <a class="lang-btn nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                    My Account</a>
                    <ul class="user_menu dropdown-menu">
                       <li><a href="/profile">Profile</a></li>
                       <li><a href="/myEarnings">My Earnings</a></li>
                       <li><a href="/myTeam">My Team</a></li>
                       <li><a href="/myTournaments">My Tournaments</a></li>
                       <li><a href="/settings">Settings</a></li>
                       <li><a href="/logout">Logout</a></li>
                    </ul>
                 </div>
             
             
          </div>
       </div>
    </div>
 </nav>
  <!-- Header Area End -->





  <!-- Game Page Start -->
  <section class="fag-game-page section_100">
    <div class="container">
      <div class="row">
        <div class="offset-md-5 col-md-4">
          <h1>My Team</h1>
          <br>
          
            
          <p style='font-family:"Roboto Condensed", sans-serif;color:orange;font-size:20px'>Status : <span style="color:red">Unverified</span> </p>
          
        </div>
      </div>
      
           <div class="row">

        <div class="col-md-8 offset-md-2 checkout-left-box">
        
          <div class="alert alert-danger" role="alert" id="err" style="display:none">

          </div>
          <div class="alert alert-success" role="alert" id="success" style="display:none">

          </div>
          
          
              <form method="post" action='/myTeam' class="needs-validation" >
                      
                  
                    <div class="form-group">
                      <label for="squadName" class="col-form-label">Squad Name</label>
                      <input type="text" class="form-control" id="squadName" name='squadName' value='Team T4T'
readonly>
<div class="invalid-feedback" id="squadNameFeedback"></div>
</div>
                  
        
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="pubgID" class="col-form-label">PUBG ID</label>
            <input type="number" class="form-control" id="leaderPubgId" name="leaderPubgId"
              value='5298730377' readonly >
            <div class="invalid-feedback" id="leaderPubgIdFeedback" ></div>
          </div>
          <div class="form-group col-md-6">
            <label for="leaderName" class="col-form-label">Leader PUBG In-Game NAME <span
                class="spinner-border spinner-border-sm spinner" style='display:none' role="status"
                aria-hidden="true"></span></label>

            <input type="text" class="form-control" id="leaderName" name='leaderName'
              value='T4T Rv' readonly>
              <div class="invalid-feedback" id="leaderNameFeedback" ></div>
    <!--          <span style="-->
    <!--position: absolute;-->
    <!--top: 48%;-->
    <!--right: 10px;-->
    <!--"><button class="fag-btn" style='padding:5px 10px'>Refresh-->
    <!--</button></span>-->
          </div>
        </div>
        
            
              
        <div class="form-row">

          <div class="form-group col-md-6">
            <label for="pubgID" class="col-form-label">PUBG ID</label>
            <input type="number" class="form-control" id="player2PubgId" name="player2PubgId"
              value='6921837322' readonly>
            <div class="invalid-feedback" id="player2PubgIdFeedback"></div>
          </div>
          <div class="form-group col-md-6">
            <label for="player2Name" class="col-form-label">PLAYER #2 PUBG In-Game NAME <span
                class="spinner-border spinner-border-sm spinner" style='display:none' role="status"
                aria-hidden="true"></span></label>
            <input type="text" class="form-control" id="player2Name" name="player2Name"
              value='T4T DeV' readonly>
              <div class="invalid-feedback" id="player2NameFeedback"></div>
    <!--          <span style="-->
    <!--position: absolute;-->
    <!--top: 48%;-->
    <!--right: 10px;-->
    <!--"><button class="fag-btn" style='padding:5px 10px'>Refresh-->
    <!--</button></span>-->

          </div>
        </div>
        
          
          
            <div class="form-row">

              <div class="form-group col-md-6">
                <label for="pubgID" class="col-form-label">PUBG ID</label>
                <input type="number" class="form-control" id="player3PubgId" name='player3PubgId'
                  value='6769074261' readonly>
                <div class="invalid-feedback" id="player3PubgIdFeedback"></div>
              </div>
              <div class="form-group col-md-6">
                <label for="player2Name" class="col-form-label">PLAYER #3 PUBG In-Game NAME <span
                    class="spinner-border spinner-border-sm spinner" style='display:none' role="status"
                    aria-hidden="true"></span></label>
                <input type="text" class="form-control" id="player3Name" name='player3Name'
                  value='T4T KiTkaT' readonly>
                  <div class="invalid-feedback" id="player3NameFeedback"></div>
        <!--          <span style="-->
        <!--position: absolute;-->
        <!--top: 48%;-->
        <!--right: 10px;-->
        <!--"><button class="fag-btn" style='padding:5px 10px'>Refresh-->
        <!--</button></span>-->
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="pubgID" class="col-form-label">PUBG ID</label>
                <input type="number" class="form-control" id="player4PubgId" name='player4PubgId'
                  value='570376686' readonly>
                <div class="invalid-feedback" id="player4PubgIdFeedback"></div>
              </div>
              <div class="form-group col-md-6">
                <label for="player4Name" class="col-form-label">PLAYER #4 PUBG In-Game NAME <span
                    class="spinner-border spinner-border-sm spinner" style='display:none' role="status"
                    aria-hidden="true"></span></label>
                <input type="text" class="form-control" id="player4Name" name='player4Name'
                  value='T4T Torq' readonly>
                  <div class="invalid-feedback" id="player4NameFeedback"></div>
        <!--          <span style="-->
        <!--position: absolute;-->
        <!--top: 48%;-->
        <!--right: 10px;-->
        <!--"><button class="fag-btn" style='padding:5px 10px' id=''>Refresh-->
        <!--</button></span>-->
              </div>
            </div>
          
            
       
      
        
             
              
              
            
        <button class='fag-btn' id='addPlayers'>[+] Add Players</button>
        <button class='fag-btn' id='addSubstitute'>[+] Add Substitute Player</button>
        <div class="form-group">
          <label for="leaderNumber" class="col-form-label">Leader's Whatsapp Number</label>
          <input type="number" class="form-control" id="leaderNumber" name="leaderNumber"
            value='8849569500' required>
        </div>
        
          <h5 style="color:red;text-align:center">Note: You can change your team only once in a month.</h5>
          <br>
            <div class="alert alert-danger" role="alert" id="err2" style="display:none">

          </div>
          <br>
  <button class="fag-btn" id="editButton">Edit existing team members</button>
        <button class="fag-btn" id="fetch">Save settings</button>




        </form>
      </div>

    </div>
      
     

    </div>
  </section>
  <div class="modal fade" id="showReasonModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Reason</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
          <p>Click on "Edit existing team members" &  Resubmit your correct details. This will be your final attempt to get your Team Verified.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
     
      </div>
    </div>
  </div>
</div>
</div>
  <!-- Game Page End -->


  <!-- Footer Area Start -->
  <footer class="fag-footer">
    <div class="footer-top-area">
       <div class="container">
          <div class="row">
             <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="single-footer">
                   <h3>About us</h3>
                   <p>Daddydope is an eSports gaming company. We organize eSports or Competitive Gaming Tournaments which are played across many platforms such as Mobile , PC.</p>
                 
                </div>
             </div>
             <div class="col-lg-5 col-md-6 col-sm-12">
                <div class="widget-content">
                   <div class="row clearfix">
                      <div class=" col-lg-6 col-md-6 col-sm-12">
                         <div class="single-footer">
                            <h3>Our Games</h3>
                            <ul>
                               <li><a href="#"><span class="fa fa-caret-right"></span>
                              Pubg Mobile</a></li>
                           
                             
                            </ul>
                         </div>
                      </div>
                      <div class=" col-lg-6 col-md-6 col-sm-12">
                         <div class="single-footer" id="footerTag">
                            <h3>Explore</h3>
                            <ul>
                               <li><a href="#"><span class="fa fa-caret-right"></span>About</a></li>
                               <li><a href="#"><span class="fa fa-caret-right"></span>Tournaments</a></li>
                               <li><a href="#"><span class="fa fa-caret-right"></span>FAQ</a></li>
                               <li><a href="#"><span class="fa fa-caret-right"></span>Privacy Policy</a></li>
                               <li><a href="#"><span class="fa fa-caret-right"></span>Terms of Service</a></li>
                            </ul>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
             <div class=" col-lg-3 col-md-6 col-sm-12">
                <div class="single-footer">
                   <h3>Contact Us</h3>
                  
                   <div class="footer-contact">
                      <h4 class="title"><i class="fa fa-pencil"></i>Email Address</h4>
                      <p>support@daddydope.in</p>
                   </div>
                   
                </div>
             </div>
          </div>
       </div>
    </div>
    <div class="footer-bottom">
       <div class="container">
          <div class="row">
             <div class="col-12">
                <div class="footer-bottom-inn">
                   <div class="footer-logo">
                      <a href="index.html">
                      <img src="assets/img/logo.png" alt="site logo" />
                      </a>
                   </div>
                   <div class="footer-social">
                      <ul>
                         <li><a href="http://facebook.com/daddydopeyt"><span class="fa fa-facebook"></span></a></li>
                         <li><a href="https://www.instagram.com/daddy_dopeyt"><span class="fa fa-instagram"></span></a></li>
                         <li><a href="https://www.youtube.com/c/DaddyDopeYT"><span class="fa fa-youtube"></span></a></li>
                         <li><a href="https://twitter.com/daddydopeyt"><span class="fa fa-twitter"></span></a></li>
                        
                      </ul>
                   </div>
                   <div class="copyright">
                      <p>&copy; Copyrights 2020 DADDYDOPE - All Rights Reserved</p>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </div>
 </footer>
  <!-- Footer Area End -->


  <!--Jquery js-->
  <script src="assets/js/jquery.min.js"></script>
  <!-- Popper JS -->
  <script src="assets/js/popper.min.js"></script>
  <!--Bootstrap js-->
  <script src="assets/js/bootstrap.min.js"></script>
 
  <!--Main js-->
  <script src="assets/js/main.js"></script>
  <script>
    if($('#playerSubPubgId').length>0){
      $('#addSubstitute').hide();
    }
  </script>
  <script>
    function removeReadonly() {
      $('input').filter((i, e) => {
        if ($(e).attr('name').search('Name') == -1) {
          return true
        }
      }).removeAttr('readonly');
      $('#leaderPubgId').attr('readonly', "1")
    }
  </script>
  <script>
    let length=$('.form-row').length;
    if($('#playerSubPubgId').length>0){
     length--;
    }
    if (length==4) {
     
      $('#addPlayers').hide();
    }

    $('#addSubstitute').on('click',(e)=>{
      e.preventDefault();
      let length = $('.form-row').length;
      let x = ` <div class="form-row">
          <div class="form-group col-md-6">
            <label for="pubgID" class="col-form-label">Substitute Player PUBG ID</label>
            <input type="number" class="form-control" id="playerSubPubgId" name="playerSubPubgId"
              value='' required>
              <div class="invalid-feedback" id="playerSubPubgIdFeedback"></div>
          </div>
          <div class="form-group col-md-6">
      <label for="playerSubName" class="col-form-label">Substitute Player PUBG In-Game NAME <span
          class="spinner-border spinner-border-sm spinner" style='display:none' role="status"
          aria-hidden="true"></span></label>
      <input type="text" class="form-control" id="playerSubName" name="playerSubName"
        value='' required>
        <div class="invalid-feedback" id="playerSubNameFeedback"></div>
        

    </div>
        </div>`
  $(x).insertAfter($('#addPlayers'));
  $('#addSubstitute').hide()
    })
    $('#addPlayers').on("click", (e) => {
      e.preventDefault()
      let length = $('.form-row').length;
      if($('#playerSubPubgId').length>0){
        length--;
      }
      if (length == 1) {
        let x = ` <div class="form-row">
          
                <div class="form-group col-md-6">
                  <label for="pubgID" class="col-form-label">PLAYER #2 PUBG ID</label>
                  <input type="number" class="form-control" id="player2PubgId" name="player2PubgId"
                    value='' required>
                    <div class="invalid-feedback" id="player2PubgIdFeedback"></div>
                </div>
                <div class="form-group col-md-6">
            <label for="player2Name" class="col-form-label">PLAYER #2 PUBG In-Game NAME <span
                class="spinner-border spinner-border-sm spinner" style='display:none' role="status"
                aria-hidden="true"></span></label>
            <input type="text" class="form-control" id="player2Name" name="player2Name"
              value='' required>
              <div class="invalid-feedback" id="player2NameFeedback"></div>
             

          </div>
              </div>`
        $(x).insertAfter($('.form-row')[length - 1])
      } else if (length == 2) {
        let x = `<div class="form-row">
                <div class="form-group col-md-6">
                  <label for="pubgID" class="col-form-label">PLAYER #3 PUBG ID</label>
                  <input type="number" class="form-control" id="player3PubgId" name="player3PubgId"
                    value='' required>
                    <div class="invalid-feedback" id="player3PubgIdFeedback"></div>
                </div>
                <div class="form-group col-md-6">
            <label for="player3Name" class="col-form-label">PLAYER #3 PUBG In-Game NAME <span
                class="spinner-border spinner-border-sm spinner" style='display:none' role="status"
                aria-hidden="true"></span></label>
            <input type="text" class="form-control" id="player3Name" name="player3Name"
              value='' required>
              <div class="invalid-feedback" id="player3NameFeedback"></div>
             

          </div>
              </div>
`
        $(x).insertAfter($('.form-row')[length - 1])

      } else if (length == 3) {
        let x=`<div class="form-row">
  <div class="form-group col-md-6">
                  <label for="pubgID" class="col-form-label">PLAYER #4 PUBG ID</label>
                  <input type="number" class="form-control" id="player4PubgId" name="player4PubgId"
                    value='' required>
                    <div class="invalid-feedback" id="player4PubgIdFeedback"></div>
                </div>
  <div class="form-group col-md-6">
            <label for="player4Name" class="col-form-label">PLAYER #4 PUBG In-Game NAME <span
                class="spinner-border spinner-border-sm spinner" style='display:none' role="status"
                aria-hidden="true"></span></label>
            <input type="text" class="form-control" id="player4Name" name="player4Name"
              value='' required>
              <div class="invalid-feedback" id="player4NameFeedback"></div>
            

          </div>
                
                
              </div>`
              
        $(x).insertAfter($('.form-row')[length - 1])
        $('#addPlayers').hide();
     
      }
    })
  </script>
  <script>
    $('#fetch').on('click', (event) => {
      event.preventDefault()
      $('form').addClass('was-validated');
      let validated = false;
      if (false){
      validated = ($("#leaderPubgId").val() != '' && $("#leaderNumber").val() != '' && $("#leaderName").val() != '' )
    }else if (3==1) {
      validated = ($("#leaderPubgId").val() != '' && $('#player2PubgId').val() != '' && $('#player2Name').val() != '' && $("#leaderNumber").val() != '')
    }else {
      validated = ($("#leaderPubgId").val() != '' && $('#player2PubgId').val() != '' && $('#player2Name').val() != '' && $('#player3PubgId').val() != '' && $('#player3Name').val() != '' && $('#player4PubgId').val() != '' && $('#player4Name').val() != '' && $("#leaderNumber").val() != '')
    }
    if($('#playerSubPubgId').length>0 && ($('#playerSubPubgId').val()=='' || $('#playerSubName').val()=='')){
      validated=false;
    }
    if (validated) {
      $('#spinner').css('display', 'flex');

      let obToSend = {};
      if (false) {
     
        if ($('.form-row').length > 1) {
          console.log('mc')
          let length = $('.form-row').length;
          if (length == 2) {
            obToSend = {
              add: 1,
              squadName: $('#squadName').val(),
                leaderNumber: $('#leaderNumber').val(),
                leaderName:$('#leaderName').val(),
                leaderPubgId: $("#leaderPubgId").val(),
                player2PubgId: $('#player2PubgId').val(),
                player2Name: $('#player2Name').val(),
                playerSubPubgId:($('#playerSubPubgId').length>0)?$('#playerSubPubgId').val():'',
                playerSubName:($('#playerSubName').length>0)?$('#playerSubName').val():''

            }
          }else if(length==3){
            if($('#player3Name').val()){
              obToSend = {
              add: 1,
           
              squadName: $('#squadName').val(),
                leaderNumber: $('#leaderNumber').val(),
                leaderPubgId: $("#leaderPubgId").val(),
                leaderName:$('#leaderName').val(),
                player2PubgId: $('#player2PubgId').val(),
                player2Name: $('#player2Name').val(),
                player3PubgId: $('#player3PubgId').val(),
                player3Name: $('#player3Name').val(),
               
            }
            }else if($('#playerSubPubgId').val()){
              obToSend = {
              add: 1,
           
              squadName: $('#squadName').val(),
                leaderNumber: $('#leaderNumber').val(),
                leaderPubgId: $("#leaderPubgId").val(),
                leaderName:$('#leaderName').val(),
                player2PubgId: $('#player2PubgId').val(),
                player2Name: $('#player2Name').val(),
               
                playerSubPubgId:($('#playerSubPubgId').length>0)?$('#playerSubPubgId').val():'',
                playerSubName:($('#playerSubName').length>0)?$('#playerSubName').val():''
            }
            }
          } else if (length == 4 || length==5) {
            obToSend = {
              add: 1,
           
              squadName: $('#squadName').val(),
                leaderNumber: $('#leaderNumber').val(),
                leaderPubgId: $("#leaderPubgId").val(),
                leaderName:$('#leaderName').val(),
                player2PubgId: $('#player2PubgId').val(),
                player2Name: $('#player2Name').val(),
                player3PubgId: $('#player3PubgId').val(),
                player3Name: $('#player3Name').val(),
                player4PubgId: $('#player4PubgId').val(),
                player4Name: $('#player4Name').val(),
                playerSubPubgId:($('#playerSubPubgId').length>0)?$('#playerSubPubgId').val():'',
                playerSubName:($('#playerSubName').length>0)?$('#playerSubName').val():''
            }
          }
        } else {
          obToSend = {
            update: 1,
            squadName: $('#squadName').val(),
                leaderNumber: $('#leaderNumber').val(),
                leaderName:$('#leaderName').val(),
                leaderPubgId: $("#leaderPubgId").val(),
                playerSubPubgId:($('#playerSubPubgId').length>0)?$('#playerSubPubgId').val():'',
                playerSubName:($('#playerSubName').length>0)?$('#playerSubName').val():''
          }

        }
      }else {
        let add = 0;

        if ($('.form-row').length == 2) {

          obToSend = {
            update: 1,
            add: add,
            squadName: $('#squadName').val(),
            leaderName: $('#leaderName').val(),
            leaderNumber: $('#leaderNumber').val(),
            leaderPubgId: $("#leaderPubgId").val(),
            player2Name: $('#player2Name').val(),
            player2PubgId: $('#player2PubgId').val(),
            playerSubPubgId:($('#playerSubPubgId').length>0)?$('#playerSubPubgId').val():'',
                playerSubName:($('#playerSubName').length>0)?$('#playerSubName').val():''

          }
        } else {
          if (3< 3) {
            if($('.form-row').length ==3){
              add = 0
            }else{
              add = 1
            }
          }
          if(3 ==3 && $('#playerSubPubgId').length>0){
            add=1
          }
          obToSend = {
            update: 1,
            add: add,
            squadName: $('#squadName').val(),
            leaderName: $('#leaderName').val(),
            leaderNumber: $('#leaderNumber').val(),
            leaderPubgId: $("#leaderPubgId").val(),
            player2Name: $('#player2Name').val(),
            player2PubgId: $('#player2PubgId').val(),
            player3Name: $('#player3Name').val(),
            player3PubgId: $('#player3PubgId').val(),
            player4Name: $('#player4Name').val(),
            player4PubgId: $('#player4PubgId').val(),
            playerSubPubgId:($('#playerSubPubgId').length>0)?$('#playerSubPubgId').val():'',
            playerSubName:($('#playerSubName').length>0)?$('#playerSubName').val():''
          }
        }

      }

console.log(obToSend)
      $.post('/updateTeam', obToSend, (data, status, xhr) => {

        if (data.pubgId) {
          $("#leaderPubgId").val('')
          $('#leaderPubgIdFeedback').text("Duplicate Leader's PubgID");
          $('#spinner').hide();
   
        } else if (data.squadName) {
          $('#squadName').val('')
          $('#squadNameFeedback').text('SquadName Already Exist')
          $('#squadNameFeedback').show()
          $('#spinner').hide();
     
        } else if (data.number) {
          $('#leaderNumber').val('')
          $('#numberFeedback').text('Number Already Exist');
          $('#spinner').hide();
        
        } else if (data.idFound) {
          let str = 'Duplicate PubgID(s):'
          let m = '';
          data.idFound.forEach((id, index) => {
            if (index == (data.idFound.length - 1)) {
              m = m + id
            } else {
              m = m + id + ','
            }
          })
          $('#err').text(str + m);
          $('.playerPubgId').val('');
          $('#err').show();
          $('#spinner').hide();
   
        } else if (data.sameIds) {
          $('#err').text("Same Ids used for multiple Players");

          $('#err').show();
          $('#spinner').hide();
         
        } else if (data.noChange) {
          $('#err').text("Nothing Changed");
          $('#err').show();
          $('#spinner').hide();
         
        } else if (data.err) {
          if(data.err==400){
            $('#err').text("Please Enter Details Correctly");
          }else{
            $('#err').text(data.err);
          }
        
          $('#err').show();
          $('#spinner').hide();
        
        } else if(data.idError){
          data.data.forEach(ob=>{
            if(ob.pubgUsername==404){
              console.log(`#${ob.player}PubgId`)
              $(`#${ob.player}PubgId`).val('');
              $(`#${ob.player}PubgId`).removeAttr('readonly')
              $(`#${ob.player}PubgIdFeedback`).text("Invalid PubgID");
              $(`#${ob.player}PubgIdFeedback`).show()
            }
          })
          $('#spinner').hide();
        }else if (data.success) {
          window.location = '/myTeam?success=1'
        } else if (data.leaderInMember) {
          $('#err').text("Leader's PubgID in Players List");
          $('#err').show();
          $('#spinner').hide();
         
        }
        else {
          for (let prop in data.result) {
            if (data.result[prop].err) {
              $(`#${prop}PubgId`).val('');
              $(`#${prop}PubgIdFeedback`).text("Invalid PubgID");
              $('#spinner').hide();
         
            } else {
              $(`#${prop}Name`).val(data.result[prop].username);
            }
          }
        }


      })
    }
    else{
   
      let m=['leader','player2','player3','player4','playerSub'];
      m.forEach(type=>{
    
        if($(`#${type}PubgId`).val()==''){
          $(`#${type}PubgIdFeedback`).text('PubgId cannot be empty')
          $(`#${type}PubgIdFeedback`).show()
        
        }
        if($(`#${type}Name`).val()==''){
          $(`#${type}NameFeedback`).text('Pubg Username cannot be empty')
          $(`#${type}NameFeedback`).show()
        }
      })
    }

        })
  </script>
<script>
    $('#editButton').on('click',(e)=>{
      e.preventDefault();
      if(-1!=-1){
        if(-1<30){
          $('#err2').text(`You will be able to change your team after 31 days`)
          $('#err2').show();
        }else{
          $('input').each((i,val)=>{
          if($(val).attr('name').search('player')>-1){
            $(val).removeAttr('readonly')
          }
        })
        }
      }else{
        $('input').each((i,val)=>{
          if($(val).attr('name').search('player')>-1){
            $(val).removeAttr('readonly')
          }
        })
      }
    })
  </script>
<script>
    $('#showReason').on('click',(e)=>{
        e.preventDefault();
        $('#showReasonModal').modal()

    })
</script>
</body>

</html>