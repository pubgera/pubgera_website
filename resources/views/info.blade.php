@extends('layouts.master')

@section('content')


  <section class="fag-game-page section_100">
    <div class="container">
      @foreach($GetTourDetails['TourDetails'] as $key => $data)
      <div class="row">
        <div class="offset-md-1 col-md-10">
          <img src="https://api.gamingmonk.com/images/games/2/newlogo.jpg" alt="game-logo" width="160" height="200" class="logo-img d-block mx-auto rounded border position-relative">
          <div
            style="background: #343434 none repeat scroll 0 0;color:white;padding:20px;border-radius: 8px;text-align: center;">
            <div>
              <h3>{{ $data['tour_type'] }}</h3>
              <p>Platform: <span><i class="fas fa-mobile-alt"></i></span></p>
            </div>
            <br>
            <div class="row">
              <div class="col-md-4">
                <h1><i class="far fa-calendar-alt"></i></h1>
                <p>{{ $data['tour_date'] }}</p>
              </div>
              <div class="col-md-4">
                <h1><i class="fas fa-trophy"></i></h1>

                <p>₹{{ $data['tour_price_pool'] }}</p>
              </div>
              <div class="col-md-4">
                <h1><i class="fas fa-user-friends"></i></h1>
                
                <p>10/{{ $data['tour_player'] }}</p>
              </div>
            </div>
            <br>
            
            <div class='row'>
              <div class='col-md-6'>
                  @if (Route::has('login')) 
                    @auth 
                    <a href="{{ URL::to('/registration/'.$data['tournament_id']) }}"><button class='fag-btn' id='participate'>Participate Now</button></a>
                  @else                 
                    <a href="/login"><button class='fag-btn' id='participate'>Login Now</button></a>
                    @endauth 
                  @endif  
              </div>
              <div class="col-md-6">
                <button class='fag-btn' data-toggle="modal" data-target="#rulesModal">Rules</button>
              </div>
            </div>                  
          </div>
        </div>
      </div>
      @endforeach
      <br>
      <div class="row">
        <div class="offset-md-1 col-md-10">
          <h3 style="text-align: center; color:white">Prize Pool</h3>
          <div style="color:white;border-radius: 8px;text-align: center;" class='single-match-widget'>
            @if($GetTourDetails['TourPricePool']->count() !== 0)
            <table class='table table-striped'  style="color:white">
              <thead>
                <tr>
                  <th scope="col">Standings</th>
                  <th scope="col">Prize</th>

                </tr>
              </thead>
              <tbody>
                @foreach($GetTourDetails['TourPricePool'] as $data)
                
                <tr>
                  <th>{{ $data->standing_no }}</th>
                  <td>₹{{ $data->price_pool }}</td>
                </tr>
                @endforeach
            </tbody>
          </table>
          @else
              <p class="text-center">Data Does Not Exists!</p>
          @endif
          </div>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="offset-md-1 col-md-10">
          <h3 style="text-align: center; color:white">Result</h3>
          <div style="color:white;border-radius: 8px;text-align: center;" class='single-match-widget'>
            @if($GetTourDetails['TourPricePool']->count() !== 0)
            <table class='table table-striped'  style="color:white">
              <thead>
                <tr>
                  <th scope="col">Standings</th>
                  <th scope="col">Prize</th>

                </tr>
              </thead>
              <tbody>
                @foreach($GetTourDetails['TourPricePool'] as $data)
                
                <tr>
                  <th>{{ $data->standing_no }}</th>
                  <td>₹{{ $data->price_pool }}</td>
                </tr>
                @endforeach
            </tbody>
          </table>
          @else
              <p class="text-center">After Tournament!</p>
          @endif
          </div>
        </div>
      </div>
    </div>
  </section>


  <!--Modal-->
  <div class="modal fade" id="rulesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">

  <!-- Custom Cursor End -->
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Rules</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style='color:white'>&times;</span>
        </button>
      </div>
      <div class="modal-body" style='color:while'>
        <p>1. Decision taken by the Admin Team will be final and binding and won't be changed or reverted under any circumstances.</p>
        <p>2. Admin Team has right to ban/kick any player without prior notice.</p>
        <p>3. Winner of each group will be awarded with the cash prize.</p>
        <p>4. Character ID (In-game Numerical ID) is not allowed.</p>
        <p>5. Do no use hacking plugins </p>
        <p>6. PC Emulator not allowed. </p>
        <p>7. No random Teamups.</p>
        <p>5. PUBG Mobile in-game name and Game ID submitted on DaddyDope should match or else the player won't receive any reward.</p>
        <p>6. Results are typically generated within few minutes  of the tournament </p>
        <p>7. Players are required to take screenshots of their match results and video recording in case they find someone cheating/teaming in the match and need to submit it as a proof at support@daddydope.in</p>
        <p><b>Note:Our team spectate each and every match & If you found Guilty we will suspend your account permanently and our decision in these are final.</b></p>
</div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="firstWarningModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">

  <!-- Custom Cursor End -->
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Rules</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style='color:white'>&times;</span>
        </button>
      </div>
      <div class="modal-body" style='color:white'>
        <p>-You are allowed to play free tournaments 3 times a week. </p>

        <p>-There are no limitation for " Paid tournaments "</p>
    </div>  
    <div class="modal-footer">
      <button type="button" class="fag-btn"  id='continue' style="width:inherit">Continue</button>
    </div>
    </div>
  </div>
</div>


<div class="modal fade" id="limitReachedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">

  <!-- Custom Cursor End -->
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Limit Reached</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style='color:white'>&times;</span>
        </button>
      </div>
      <div class="modal-body" style='color:while'>
        <p>- You have reached your weekly limit! </p>
        <p>- Sorry you cannot register for this tournament because we allow players/team to play “free tournaments” only 3 times a week. </p>
        <p>- To monitor your weekly limits go to <a href='signin.html' style="color:orange">My Profile</a></p>
        <p><b>NOTE: Do not worry you can still register yourself in Paid Tournaments <a href='tournaments.html' style="color:orange">Click Here</a></b></p>
      </div>
      
    </div>
  </div>
</div>
@stop