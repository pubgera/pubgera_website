<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="description" content="DaddyDope | ESports gaming company">
    <meta name="keyword" content="game, gaming, videogame, developer, steam, studio, team">
    
    <!-- Title -->
    <title>My Tournament</title>
    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon/favicon-32x32.png">
    <!--Bootstrap css-->
    <link href="{{URL::asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assets/css/datatables.min.css')}}" rel="stylesheet">
    
    <!--Font Awesome css-->
    <link href="{{URL::asset('assets/fontawesome/css/all.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assets/css/font-awesome.min.css')}}" rel="stylesheet">
    
    <!--Magnific css-->
    <link href="{{URL::asset('assets/css/magnific-popup.css')}}" rel="stylesheet">
    
    <!--Owl-Carousel css-->
    <link href="{{URL::asset('assets/css/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assets/css/owl.theme.default.min.css')}}" rel="stylesheet">
    
    <!--NoUiSlider css-->
    <link href="{{URL::asset('assets/css/nouislider.min.css')}}" rel="stylesheet">
    
    <!--Animate css-->
    <link href="{{URL::asset('assets/css/animate.min.css')}}" rel="stylesheet">
    
    <!--Site Main Style css-->
    <link href="{{URL::asset('assets/css/style.css')}}" rel="stylesheet">
    
    <!--Responsive css-->
    <link href="{{URL::asset('assets/css/responsive.css')}}" rel="stylesheet">
    
    <style>
        .form-control[readonly]{
            background-color:#818181 !important;
        }
    </style>
 </head>