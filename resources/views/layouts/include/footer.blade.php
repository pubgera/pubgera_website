<footer class="fag-footer">
    <div class="footer-top-area">
       <div class="container">
          <div class="row">
             <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="single-footer">
                   <h3>About us</h3>
                   <p>Daddydope is an eSports gaming company. We organize eSports or Competitive Gaming Tournaments which are played across many platforms such as Mobile , PC.</p>
                 
                </div>
             </div>
             <div class="col-lg-5 col-md-6 col-sm-12">
                <div class="widget-content">
                   <div class="row clearfix">
                      <div class=" col-lg-6 col-md-6 col-sm-12">
                         <div class="single-footer">
                            <h3>Our Games</h3>
                            <ul>
                               <li><a href="#"><span class="fa fa-caret-right"></span>
                              Pubg Mobile</a></li>
                           
                             
                            </ul>
                         </div>
                      </div>
                      <div class=" col-lg-6 col-md-6 col-sm-12">
                         <div class="single-footer" id="footerTag">
                            <h3>Explore</h3>
                            <ul>
                               <li><a href="#"><span class="fa fa-caret-right"></span>About</a></li>
                               <li><a href="#"><span class="fa fa-caret-right"></span>Tournaments</a></li>
                               <li><a href="#"><span class="fa fa-caret-right"></span>FAQ</a></li>
                               <li><a href="#"><span class="fa fa-caret-right"></span>Privacy Policy</a></li>
                               <li><a href="#"><span class="fa fa-caret-right"></span>Terms of Service</a></li>
                            </ul>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
             <div class=" col-lg-3 col-md-6 col-sm-12">
                <div class="single-footer">
                   <h3>Contact Us</h3>
                  
                   <div class="footer-contact">
                      <h4 class="title"><i class="fa fa-pencil"></i>Email Address</h4>
                      <p>support@daddydope.in</p>
                   </div>
                   
                </div>
             </div>
          </div>
       </div>
    </div>
    <div class="footer-bottom">
       <div class="container">
          <div class="row">
             <div class="col-12">
                <div class="footer-bottom-inn">
                   <div class="footer-logo">
                      <a href="index-2.html">
                      <img src="assets/img/logo.png" alt="site logo" />
                      </a>
                   </div>
                   <div class="footer-social">
                      <ul>
                         <li><a href="http://facebook.com/daddydopeyt"><span class="fa fa-facebook"></span></a></li>
                         <li><a href="https://www.instagram.com/daddy_dopeyt"><span class="fa fa-instagram"></span></a></li>
                         <li><a href="https://www.youtube.com/c/DaddyDopeYT"><span class="fa fa-youtube"></span></a></li>
                         <li><a href="https://twitter.com/daddydopeyt"><span class="fa fa-twitter"></span></a></li>
                        
                      </ul>
                   </div>
                   <div class="copyright">
                      <p>&copy; Copyrights 2020 DADDYDOPE - All Rights Reserved</p>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </div>
 </footer>
      <!-- Footer Area End -->
       <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

        
