<nav class="fag-header navbar navbar-expand-lg">
    <div class="container">
       <a class="navbar-brand" href="index.html"><img src="assets/img/logo.png" alt="site logo" /></a>
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
       <span class="menu-toggle"></span>
       </button>
       <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="header_menu  mr-auto">
             <li class="nav-item active">
                <a href="/" class="nav-link">Home</a>
             </li>
             <li class="nav-item active">
               <a href="/" class="nav-link">Tournaments</a>
            </li>
             <li class="nav-item">
                <a href="#footerTag" class="nav-link">Contact</a>
             </li>
          </ul>
          <div class="header-right  my-2 my-lg-0">
           @if (Route::has('login')) 
                @auth
                <div class="nav-item dropdown">
                      <a class="lang-btn nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                      My Account</a>
                      <ul class="user_menu dropdown-menu">
                         <li><a href="/profile">Profile</a></li>
                         <li><a href="/myEarnings">My Earnings</a></li>
                         <li><a href="/myTeam">My Team</a></li>
                         <li><a href="/myTournaments">My Tournaments</a></li>
                         <li><a href="/settings">Settings</a></li>
                         
                          <li>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>              
                      </ul>
                </div>                
            @else                    
                <div class="header-auth nav-item">
                    <ul class="header_menu">
                        <li class="nav-item active">
                            <a href="/login">Log In</a>
                         </li>
                          <li class="nav-item active">
                            <a href="/register">Sign Up</a>
                         </li>
                    </ul>
                 </div> 
                @endauth 
            @endif                           
          </div>
       </div>
    </div>
 </nav>
      <!-- Header Area End -->