<!doctype html>
<html lang="en">
  	<head>
		@include('layouts.include.head')
  	</head>

  	<body>
    	<div class="app">
	  		<div class="layout">
	    		<!-- Header START -->
	    		@include('layouts.include.header')
	            <!-- Header END -->

		  		<!-- Page Container START -->
		        <div class="page-container">
		        	<!-- Content Wrapper START -->
		        	<div class="main-content">
	            		@yield('content')
		            </div>

			  		<!-- Footer START -->
			        @include('layouts.include.footer')
			        <!-- Footer END -->
		        </div>
		        <!-- Page Container END -->
        	</div>
        </div>
        @yield('customJs')

  	</body>
</html>
